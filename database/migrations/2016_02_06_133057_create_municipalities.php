<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMunicipalities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('municipalities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('RegionCode');
            $table->string('ProvinceCode', 20);
            $table->string('MunicipalityCode', 20);
            $table->string('MunicipalityName');
            $table->string('MunicipalityLatitude');
            $table->string('MunicipalityLongitude');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('municipalities');
    }
}
