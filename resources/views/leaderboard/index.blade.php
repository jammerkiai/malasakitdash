@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success" role="alert">
                    {{ Session::get('message') }}
                    </div>
                @endif
                
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Leader Board - Overall Standings</h4>
                    </div>
                    <div class="panel-body">
                        @include('leaderboard.board', ['results' => $overall])
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Leader Boards - This week's standings</h4>
                    </div>
                    <div class="panel-body">
                        @include('leaderboard.board', ['results' => $thisweek])
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Leader Boards - Last week's standings</h4>
                    </div>
                    <div class="panel-body">
                        @include('leaderboard.board', ['results' => $lastweek])
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection