@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success" role="alert">
                    {{ Session::get('message') }}
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">Entries
                        <div class="pull-right">
                            <a href="{{ url('entries/create') }}" class="">Add New Entry</a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-responsive">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>DATE</th>
                                <th>REPORTER</th>
                                <th>TASK</th>
                                <th>LOCATION</th>
                                <th>COORDS</th>
                                <th>ACTION</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($reports as $report)
                                <tr>
                                    <td>{{ $report->id }}
                                    <td>{{ $report->reportDate }}</td>
                                    <td> 
                                    {!! App\Models\Coordinator::find($report->coordinatorId)['firstname'] !!}
                                    {!! App\Models\Coordinator::find($report->coordinatorId)['lastname'] !!}</td>
                                    <td>{!! App\Models\Task::find($report->task_id)['name'] !!}</td>
                                    <td>
                                        {{ $report->barangay or ''}}
                                    </td>
                                    <td>
                                        {{ $report->latitude or ''}}, {{ $report->longitude or ''}}
                                    </td>
                                    <td>
                                        <a class="btn btn-sm btn-primary" href="{{ url('entries', $report->id) }}">View</a>
                                    </td>
                                </tr>
                            @empty
                                <tr><td colspan="8">No reports</td></tr>
                            @endforelse
                            </tbody>
                            <tfoot>
                            <tr><td colspan="8"><center>{!! $reports->links() !!}</center></td></tr>
                            </tfoot>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
