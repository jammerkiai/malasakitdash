@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit Coordinator Details</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST"
                              action="{{ url('/reporters', $coordinator->id) }}">
                            <input type="hidden" value="PATCH" name="_method" />
                            <input type="hidden" value="auth" name="segment" />
                            <input type="hidden" value="{{ $coordinator->id }}" name="id" />
                            {!! csrf_field() !!}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input type="email" class="form-control"
                                           name="email" value="{{ $coordinator->email }}">

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Password</label>

                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password">

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Confirm Password</label>

                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password_confirmation">

                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <a href="{{  url('reporters') }}" class="btn btn-warning">
                                        <i class="fa fa-btn fa-arrow-left"></i>Back to List
                                    </a>
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-btn fa-save"></i>Update Password
                                    </button>
                                </div>
                            </div>
                            </form>
                            <hr />
                            <form class="form-horizontal" role="form" method="POST"
                                  action="{{ url('/reporters', $coordinator->id) }}">
                                <input type="hidden" value="PATCH" name="_method" />
                                <input type="hidden" value="personal" name="segment" />
                                <input type="hidden" value="{{ $coordinator->id }}" name="id" />
                                {!! csrf_field() !!}

                            <div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">First Name</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control"  id="firstname"
                                           name="firstname" value="{{ $coordinator->firstname }}">

                                    @if ($errors->has('firstname'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('middlename') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Middle Name</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control"  id="middlename"
                                           name="middlename" value="{{$coordinator->middlename }}">

                                    @if ($errors->has('middlename'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('middlename') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Last Name</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control"  id="lastname"
                                           name="lastname" value="{{ $coordinator->lastname}}">

                                    @if ($errors->has('lastname'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Mobile</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control"  id="mobile"
                                           name="mobile" value="{{ $coordinator->mobile }}">

                                    @if ($errors->has('mobile'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('birthday') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Birthday</label>

                                <div class="col-md-6">
                                    <input type="date" class="form-control"  id="birthday"
                                           name="birthday" value="{{ $coordinator->birthday }}">

                                    @if ($errors->has('birthday'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('birthday') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Gender</label>
                                <div class="col-md-6">
                                    <select class="form-control" name="gender">
                                        <option value="Male" @if($coordinator->gender=='Male') selected @endif>Male</option>
                                        <option value="Female" @if($coordinator->gender=='Female') selected @endif>Female</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Position</label>
                                <div class="col-md-6">
                                    <select class="form-control" name="position">
                                        <option value="Team Leader" @if($coordinator->position=='Team Leader') selected @endif>Team Leader</option>
                                        <option value="Asst Team Leader" @if($coordinator->position=='Asst Team Leader') selected @endif>Asst Team Leader</option>
                                        <option value="Member" @if($coordinator->position=='Member') selected @endif>Member</option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('active') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Status</label>
                                <div class="col-md-6">
                                    <select class="form-control" name="active">
                                        <option value="1" @if($coordinator->active==1) selected @endif>Active</option>
                                        <option value="0" @if($coordinator->active!=1) selected @endif>Inactive</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <a href="{{  url('reporters') }}" class="btn btn-warning">
                                        <i class="fa fa-btn fa-arrow-left"></i>Back to List
                                    </a>
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-btn fa-save"></i>Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('customjs')
    <script>

        $(document).ready(function(){
            $('#name').trigger('focus');
        });

    </script>
@endsection
