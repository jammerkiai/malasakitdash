<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = 'tasks';
    protected $fillable = [ 'name', 'active' ];

    public function report() {
        return $this->hasMany('App\Models\Report', 'id', 'task_id');
    }
}
