<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

class LeaderBoardController extends Controller
{
    public function index() {

    	$overall = $thisweek = $lastweek = [];

    	$overall = $this->getResults();

    	$start = new \DateTime('last monday');
    	$end = new \DateTime('next sunday');
    	$thisweek = $this->getResults($start->format('Y-m-d'), $end->format('Y-m-d'));

		$start->sub(new \DateInterval('P7D'));
    	$end->sub(new \DateInterval('P7D'));
    	$lastweek = $this->getResults($start->format('Y-m-d'), $end->format('Y-m-d'));


    	return view('leaderboard.index', compact('overall', 'thisweek', 'lastweek'));

    }

    protected function getResults($start = null, $end = null) {

    	$sql = "select concat(c.firstname,' ', c.lastname) as coordinatorname, 
    			(select count(*) from reports r where r.coordinatorId=c.id) as reports, 
    			(select count(*) from contacts c1 where c1.coordinatorId=c.id) as contacts 
    			from coordinators c";

    	if (!is_null($start) && !is_null($end)) {
    		$sql = "select concat(c.firstname,' ', c.lastname) as coordinatorname, 
    			(select count(*) from reports r where r.coordinatorId=c.id and r.reportDate between '$start' and '$end') as reports, 
    			(select count(*) from contacts c1 where c1.coordinatorId=c.id and c1.created_at between '$start' and '$end') as contacts 
    			from coordinators c";
    	}

    	$overall = $this->processResults(DB::select($sql));
    	array_multisort($overall, SORT_DESC);

    	return $overall;
    }

    protected function processResults($results) {
    	$out = [];
    	foreach ($results as $res) {
    		$tmp = [
    			'total' => $res->reports + $res->contacts,
    			'coordinatorname' => $res->coordinatorname,
    			'reports' => $res->reports,
    			'contacts' => $res->contacts,
    		];

    		if ($tmp['total'] > 0) {
    			array_push($out, $tmp);
    		}

    	}
    	return $out;
    }
}
