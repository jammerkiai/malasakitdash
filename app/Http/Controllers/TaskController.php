<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Task;
use Session;


class TaskController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {

        $tasks = Task::all();
        return view('tasks.index', compact('tasks'));
    }

    public function create() {
        return view('tasks.create');
    }

    public function edit($id) {

        $task = Task::findOrFail($id);

        return view('tasks.edit', compact('task'));

    }

    public function update($id, Request $request) {

        $task = Task::findOrFail($id);

        $this->validate($request, [
            'name' => 'required'
        ]);

        $input = $request->all();

        $task->fill($input)->save();

        Session::flash('flash_message', 'Task successfully added!');

        return redirect()->back();

    }

    public function store(Request $request) {
        $this->validate($request,[
           'name' => 'required'
        ]);

        $input = $request->all();
        Task::create($input);

        Session::flash('flash_message', 'Task successfully added!');

        return redirect()->back();
    }

    public function destroy($id) {
        $task = Task::findOrFail($id);
        $task->delete();
        Session::flash('flash_message', 'Task successfully deleted!');

        return redirect()->route('tasks.index');
    }
}
